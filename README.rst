How to use this boilerplate for your new project
------------------------------------------------

Just copy or pull all the boilerplate files to your new repository and set your
new remote.

.. code-block::

    cd my-project
    git init
    git pull git@bitbucket.org:market_app/kaiju-app.git


Compatibility
-------------

**Python**: 3.8

Summary
-------

- **application.py** - app init function used in `__main__.py`

Testing
-------

At first, you must install `requirements.tests.txt` as well
as normal requirements.

pytest
^^^^^^

Run `pytest` command. There's also a Pycharm *unittests*
run configuration ready to use.

tox
^^^

To test with tox you should install and use `pyenv`. First
setup local interpreters which you want to use in tests.

```pyenv local 3.7.5 3.8.1 3.9.0```

Then you can run `tox` command to test against all of them.

Documentation
-------------

sphinx
^^^^^^

Install `requirements.docs.txt` and then
cd to `./docs` and run `make html` command. There is also a
run configuration for Pycharm.

Build
-------------
pre install for linux:
    sudo apt-get install -y chrpath

    # sudo apt-get install clang
    sudo  apt-get install clang-7 clang-tools-7 clang-7-doc libclang-common-7-dev libclang-7-dev libclang1-7 clang-format-7 python-clang-7


install

    pip install -U "https://github.com/Nuitka/Nuitka/archive/factory.zip"

Run
    python3 -m nuitka --standalone  --include-module=asyncpg.pgproto -j 8 app --clang --show-progress --show-memory
    ./app

run by script:

. build.sh
