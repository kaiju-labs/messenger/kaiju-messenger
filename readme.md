## Requirements:
* postgresql
* kafka
* keydb / redis
* elasticsearch


## Ubuntu:
` sudo apt-get install -y chrpath`

## Приложение состоит из 4 компонентов:
* main app
* manager
* executor
* kafka listener

Настрйоки приложения лежат в settings

## Запуск v2
Install search:

Запуск виртуального окружения:  
`python3 -m 'venv' venv`  
`venv/bin/pip install -r requirements.txt`  

`sudo docker-compose up elasticsearch`  ( если не установлен локально ) 
`sudo docker-compose up redis` ( если не установлен локально ) 

Для всех:  `sudo docker-compose up kafka`
Для m1: `sudo docker-compose up kafka_m1`  

В отдельных вкладках:


`./venv/bin/python -m app`  
`./venv/bin/python -m app kafka_listener`  
`./manager.sh`  
`./executor.sh`  



## Запуск main app
в директории elemento.search запускаем:

`./app`

в `settings/` лежат конфиги для основного приложения

настроки в env.json



## Запуск manager
в директории elemento.search запускаем:

`./app --env-file=settings/manager/env.json --cfg=settings/manager/config.yml --no-base-cfg`

в `settings/manager/` лежат конфиги для manager



## Запуск executor
в директории elemento.search запускаем:

`./app --env-file=settings/executor/env.json --cfg=settings/executor/config.yml --no-base-cfg`

в `settings/executor` лежат конфиги для executor

executor - должен иметь доступ к manager, т.к. происходит синхронизация.


## Запуск kafka listener
в директории elemento.search запускаем:

`./app kafka_listener`


kafka listener - закрытое приложение, ему нужен доступ только к kafka


## Обновление бд




SET maintenance_work_mem TO '4 GB';