import asyncio
import uuid as uuid
from kaiju_tools.fixtures import ContextableService
from kaiju_tools.encoding.json import dumps, loads
import re

class RedisHandler(ContextableService):
    service_name = "RedisHandler"

    def __init__(self, redis_sub_service, websocket_server_service, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._redis_sub = None
        self._ws_server = None
        self.background_tasks = set()
        self._redis_sub_name = redis_sub_service
        self.ws_server_name = websocket_server_service
        self.subscribe_dict = dict()

    async def init(self):
        self._ws_server = self.discover_service(self.ws_server_name, cls=None, required=True)
        self._redis_sub = self.discover_service(self._redis_sub_name, cls=None, required=True)
        await self._redis_sub.subscribe("SERVICE")

    async def handle(self, data):
        channel = data["channel"].decode('UTF-8')
        self.logger.debug('HANDLER "%s".', channel)
        if channel == "SERVICE":
            temp_task = asyncio.create_task(self._service_handler(data))
        elif re.match(r'USER_INFO', 'channel'):
            temp_task = asyncio.create_task(self._service_handler(data))
        else:
            temp_task = asyncio.create_task(self._send_clients_by_chat(data))

        self.background_tasks.add(temp_task)
        temp_task.add_done_callback(self.background_tasks.discard)

    async def callback(self, data):
        if data['method'] == 'chat.delete':
            await self.unsubscribe_all(data['data']['chat_id'])

        elif data['method'] == 'chat.leave':
            await self.unsubscribe(data['data']['chat_id'], data['data']['client_id'])

    async def _send_clients_by_chat(self, data):
        chat_id = data["channel"].decode('UTF-8')
        if chat_id in self.subscribe_dict:
            temp = loads(data["data"].decode('UTF-8'))
            for i in self.subscribe_dict[chat_id]:
                for j in self._ws_server[i]:
                    if not j.closed:
                        await j.send_json(temp, dumps=dumps)
            await self.callback(temp)

    async def _client_info_handler(self, data):
        temp = loads(data["data"].decode('UTF-8'))
        clients_id = data["data"]['clients_id']
        if clients_id in self._ws_server:
            pass
    async def _service_handler(self, data):
        temp = loads(data["data"].decode('UTF-8'))
        await self.subscribe(str(temp["data"]['chat_id']), temp["data"]['user_id'])
        if temp["data"]['user_id'] in self._ws_server:
            for i in self._ws_server.get_worker(temp["data"]['user_id']):
                await i.new_chat(temp["data"]['chat_id'])

    async def subscribe(self, chat_id, user_id):
        if isinstance(chat_id, str):
            if user_id in self._ws_server:
                if chat_id in self.subscribe_dict:
                    self.subscribe_dict[chat_id].add(user_id)
                else:
                    await self._redis_sub.subscribe(chat_id)
                    self.subscribe_dict[chat_id] = set()
                    self.subscribe_dict[chat_id].add(user_id)

        if isinstance(chat_id, list):
            if user_id in self._ws_server:
                for i in chat_id:
                    if i in self.subscribe_dict:
                        self.subscribe_dict[i].add(user_id)
                    else:
                        await self._redis_sub.subscribe(i)
                        self.subscribe_dict[i] = set()
                        self.subscribe_dict[i].add(user_id)

    async def unsubscribe(self, chat_id, user_id):
        if chat_id in self.subscribe_dict:
            if user_id in self.subscribe_dict[chat_id] and not self._ws_server[user_id]:
                self.subscribe_dict[chat_id].remove(user_id)
            if not self.subscribe_dict[chat_id]:
                self.subscribe_dict.pop(chat_id)
                self.logger.info(self.subscribe_dict)
                await self._redis_sub.unsubscribe(chat_id)

    async def unsubscribe_all(self, chat_id):
        if str(chat_id) in self.subscribe_dict:
            self.logger.debug("Callback chat delete %s", chat_id)
            self.subscribe_dict.pop(str(chat_id))
            await self._redis_sub.unsubscribe(chat_id)

    @property
    def service_info(self):
        return {
            "background_tasks": self.background_tasks,
            "dict_subscribe": self.subscribe_dict
        }