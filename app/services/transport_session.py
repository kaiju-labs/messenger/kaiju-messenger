from typing import List, Union
import uuid
import logging

from kaiju_tools.encoding.json import dumps
from kaiju_tools.exceptions import NotAuthorized
from kaiju_tools.fixtures import ContextableService
from kaiju_auth import JWTService

from .group_service import MessengerGroupService
from .types import MessageInfo
from .core_service import MessageService

__all__ = ["TransportSessionService"]
STATUS = {
    "OK": "OK",
    "Not Chat": "NOT CHAT",
    "CHAT DELETE": "CHAT DELETE",
    "Not owner": "NOT OWNER",
    "NOT ALLOWED": "NOT ALLOWED",

}


class TransportSessionService(ContextableService):
    service_name = "TransportSession"
    token_service_class = JWTService
    group_service: MessengerGroupService
    message_service: MessageService

    def __init__(
            self, app,
            token_service: Union[str, token_service_class] = None,
            group_service: str = None,
            core_service: str = None,
            client_service: str = None,
            redis_pub_service: str = None,
            redis_handler: str = None,
            logger=None, **_):
        super().__init__(app=app, logger=logger)
        self._caller_client = None
        self._token_service_name = token_service
        self._token_service = None
        self._caller_client = self.discover_service(client_service, cls=None, required=True)
        self._redis_pub = self.discover_service(redis_pub_service, cls=None, required=True)
        self.redis_handler = self.discover_service(redis_handler, cls=None, required=True)
        self.message_service = self.discover_service(core_service, cls=None, required=True)
        self.group_service = self.discover_service(group_service, cls=None, required=True)

    @property
    def routes(self):
        return {
            'chat.create': self.chat_create,
            'chat.list': self.chat_list,
            'chat.leave': self.chat_leave,
            'chat.invite_user': self.chat_invite,
            'chat.delete': self.delete_chat,
            'chat.new_name': self.group_service.update_name,
            'chat.settings': self.group_service.settings,
            'chat.change_owner': self.group_service.change_owner,
            'message.send': self.message_service.send,
            'message.list': self.message_service.list,
            'message.list_by_id': self.message_service.list_by_message_id,
            'message.delete': self.message_service.delete,
            'message.update': self.message_service.update,
            'message.mark_as_read': self.message_service.mark_as_read,
            'message.mark_as_arrived': self.message_service.mark_as_arrived,
            'message.search_by_tags': self.message_service.search_by_tags,
            'attachment.list': self.attachment_list,
        }

    @property
    def redirected_method(self):
        return [
            'chat.create',
            'chat.leave',
            'chat.invite_user',
            'chat.new_name',
            'chat.settings',
            'chat.delete',
            'chat.change_owner',
            'message.send',
            'message.delete',
            'message.update',
            'message.mark_as_read',
            'message.mark_as_arrived'
        ]

    async def init(self):
        self.app.services.messenger_transport.logger.setLevel(logging.ERROR)
        self.app.services.messenger_chat_transport.logger.setLevel(logging.ERROR)
        self.app.services.messenger_client_transport.logger.setLevel(logging.ERROR)
        self._token_service = self.discover_service(
            self._token_service_name, cls=self.token_service_class, required=False)

    async def chat_create(self, type_chat: str, user_id: uuid.UUID, chat_name: str = "", inviting_clients=None):
        client_list = None
        if inviting_clients is not None:
            if type(inviting_clients) is str:
                inviting_clients = [inviting_clients]
            client_id_info = await self._caller_client.call(method="Clients.user_id_by_email",
                                                            params={"email": inviting_clients})
            if client_id_info['status'] != "OK":
                return client_id_info
            client_list = client_id_info['data']['user_id']
        data = {'method': 'chat.create',
                **(await self.group_service.create(chat_name=chat_name,
                                                   type_chat=type_chat,
                                                   user_id=user_id,
                                                   inviting_clients=client_list))
                }
        if data['status'] == 'OK':
            if inviting_clients is not None:
                for client_id in inviting_clients:
                    temp = {"method": "chat.invite", "data": {"chat_id": str(data["data"]["chat_id"]),
                                                              "user_id": client_id}}
                    await self._redis_pub.publish("SERVICE", dumps(temp))
            params = {
                'chat_id': data["data"]["chat_id"],
                'data': "Chat.create.message"
            }
            temp_first_send = await self.message_service.call(method="Messenger.message.send_first", params=params)
            temp_first_send["method"] = "message.send"
            await self.redis_handler.subscribe(str(data["data"]["chat_id"]), user_id)
            await self._redis_pub.publish(str(data["data"]["chat_id"]), dumps(temp_first_send))
            if data['data']['clients_list']:
                client_list = await self._caller_client.call(method="Clients.user_public_info",
                                                             params={"user_id": data['data']['clients_list']})
                data['data']['clients_list'] = client_list['data']
        return data

    async def chat_leave(self, user_id: uuid.UUID, chat_id: uuid.UUID):
        data = await self.group_service.leave(user_id=user_id, chat_id=chat_id)
        if data['status'] == "CHAT DELETE":
            await self.message_service.call(method="Messenger.message.delete_all", params={"chat_id": chat_id})
            await self.message_service.call(method="Messenger.attachment.delete_all", params={"chat_id": chat_id})
            data['status'] = STATUS["OK"]
        return data

    async def chat_list(self, user_id: uuid.UUID):
        data_chat_service = await self.group_service.list(user_id)
        clients_set = set()
        chats = []
        for i in data_chat_service['data']['chat_list']:
            clients_set.update(i['clients_list'])
            chats.append({"chat_id": i['chat_id'], "type": i['type']})
        temp = list(clients_set)
        if temp:
            client_list = await self._caller_client.call(method="Clients.user_public_info",
                                                         params={"user_id": temp})
            data_chat_service['data']['clients_list'] = client_list['data']
            self.logger.info(data_chat_service)
        data_chat_service['status'] = STATUS['OK']
        data_chat_service['data']['chats'] = chats
        return data_chat_service

    async def chat_list_without_clients_info(self, user_id):
        data_chat_service = await self.group_service.list(user_id)
        return data_chat_service

    async def chat_invite(self, user_id: uuid.UUID, email: str, chat_id: uuid.UUID):
        client_data = await self._caller_client.call(method="Clients.user_id_by_email",
                                                     params={"email": email})

        if client_data["status"] == STATUS['OK']:
            inviting_user_id = client_data["data"]["user_id"]
            # await self.redis_handler.subscribe(str(chat_id), inviting_user_id)
            data = await self.group_service.invite(inviting_user_id=inviting_user_id,
                                                   user_id=user_id,
                                                   chat_id=chat_id)
            if data["status"] == STATUS['OK']:
                temp = {"method": "chat.invite", "data": {"chat_id": str(chat_id), "user_id": inviting_user_id}}
                await self._redis_pub.publish("SERVICE", dumps(temp))
                if data['data']['clients_list']:
                    client_list = await self._caller_client.call(method="Clients.user_public_info",
                                                                 params={"user_id": data['data']['clients_list']})
                    data['data']['clients_list'] = client_list['data']
            return data
        else:
            return client_data

    async def delete_chat(self, user_id: uuid.UUID, chat_id: uuid.UUID):
        data = await self.group_service.delete(user_id=user_id, chat_id=chat_id)
        if data['status'] == 'OK':
            await self.message_service.call(method="Messenger.message.delete_all", params={"chat_id": chat_id})
            await self.message_service.call(method="Messenger.attachment.delete_all", params={"chat_id": chat_id})
        return data

    async def chat_update_name(self, user_id: uuid.UUID, chat_id: uuid.UUID, new_name: str):
        data = await self.group_service.update_name(user_id=user_id, chat_id=chat_id, new_name=new_name)
        return data

    async def chat_info(self, chat_id: uuid.UUID):
        data = await self.group_service.info(chat_id=chat_id)
        return data

    async def send_message(self, user_id: uuid.UUID, chat_id: uuid.UUID, data: str,
                           attachment: list = None,
                           tags: List[str] = None):
        data = await self.message_service.send(chat_id=chat_id,
                                               data=data,
                                               user_id=user_id,
                                               attachment=attachment,
                                               tags=tags
                                               )
        return data

    async def message_list(self, user_id: uuid.UUID, chat_id: uuid.UUID, paging: bool = False):
        data = await self.message_service.list(user_id=user_id, chat_id=chat_id, paging=paging)
        return data

    async def message_list_by_message_id(self, chat_id: uuid.UUID, message_info: MessageInfo, user_id: uuid.UUID,
                                         direction: bool = True):
        data = await self.message_service.list_by_message_id(chat_id=chat_id,
                                                             message_info=message_info,
                                                             user_id=user_id,
                                                             direction=direction)
        return data

    async def delete_message(self, chat_id: uuid.UUID, user_id: uuid.UUID, message_info: MessageInfo):
        data = await self.message_service.delete(user_id=user_id, chat_id=chat_id, message_info=message_info)
        return data

    async def message_update(self, user_id: uuid.UUID, chat_id: uuid.UUID, message_info: MessageInfo, data: str):
        data = await self.message_service.update(user_id=user_id, chat_id=chat_id, message_info=message_info, data=data)
        return data

    async def message_mark_as_read(self, chat_id: uuid.UUID, user_id: uuid.UUID, message_info: MessageInfo):
        data = await self.message_service.mark_as_read(chat_id=chat_id, user_id=user_id, message_info=message_info)
        return data

    async def message_mark_as_arrived(self, chat_id: uuid.UUID, user_id: uuid.UUID, message_info: MessageInfo):
        data = await self.message_service.mark_as_arrived(chat_id=chat_id, user_id=user_id, message_info=message_info)
        return data

    async def message_search_by_tags(self, chat_id: uuid.UUID, message_info: MessageInfo, tag: str):
        data = await self.message_service.search_by_tags(chat_id=chat_id, message_info=message_info, tag=tag)
        return data

    async def attachment_list(self, **kwargs):
        data = await self.message_service.call(method="Messenger.attachment.list", params=kwargs)
        return data

    async def login(self, access_token):
        if access_token is None:
            return {"data": "Uncorrected token", "status": "Uncorrected token"}, None, None
        try:
            token = await self._token_service.verify_token(access_token)
            user_id = token.claims_data['user_id']
        except NotAuthorized:
            user_id = None
        if user_id is None:
            return {"data": "Uncorrected token", "status": "Uncorrected token"}, None, None
        else:
            chat_list = await self.group_service.list(user_id)
            chat_list_str = []
            for i in chat_list['data']['chat_list']:
                chat_list_str.append(str(i["chat_id"]))
            return {"data": "Correct token", "status": "OK"}, user_id, chat_list_str
