import asyncio

from kaiju_tools.fixtures import ContextableService
from typing import Union, Optional
import logging

from kaiju_redis.transport import RedisTransportService


class RedisSubService(ContextableService):
    service_name = "RedisSub"
    transport_cls: RedisTransportService = None
    refresh_rate = 0.001

    def __init__(self, app, transport: str, handle_class_name: str, refresh_rate: float = 0.001,
                 logger=None):
        super().__init__(app=app, logger=logger)
        self.transport_name = transport
        self.refresh_rate = refresh_rate
        self._transport = None
        self._pubsub = None
        self._task = None
        self.callback_function = None
        self.handle_class_name = handle_class_name
        self.subscribe_dict = {}
        self.class_handle = None
        self._transport = self.discover_service(self.transport_name, cls=RedisTransportService, required=True)
        self._pubsub = self._transport.pubsub()

    @property
    def closed(self) -> bool:
        return self._task is None

    async def close(self):
        if not self.closed:
            self._task.cancel()
            self._task = None

    async def init(self):

        self.class_handle = self.discover_service(self.handle_class_name, cls=None, required=True)

        self._task = asyncio.create_task(self._loop())
        self.logger.info("Run loop")

    async def _loop(self):
        while True:
            if self._pubsub.subscribed:
                try:
                    message = await self._pubsub.get_message(ignore_subscribe_messages = True, timeout=1)
                    if message:
                        await self.class_handle.handle(message)
                except:
                    self.logger.debug("Redis sub error")
            await asyncio.sleep(self.refresh_rate)

    async def subscribe(self, chat_id):
        self.logger.debug("Subscribe %s", chat_id)
        await self._pubsub.subscribe(chat_id)

    async def unsubscribe(self, chat_id):
        self.logger.debug("Unsubscribe %s", chat_id)
        await self._pubsub.unsubscribe(chat_id)


class RedisPubService(ContextableService):
    service_name = "RedisPub"
    transport_cls: RedisTransportService = None

    def __init__(self, app, transport: str, logger=None):
        super().__init__(app=app, logger=logger)
        self.transport_name = transport
        self._transport = None

    async def init(self):
        self._transport = self.discover_service(self.transport_name, cls=RedisTransportService, required=True)

    async def publish(self, chat_id, data):
        self.logger.info("Publish %s", chat_id)
        await self._transport.publish(chat_id, data)



