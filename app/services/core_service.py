import uuid
import datetime
from typing import List, Union
from .types import MessageInfo, Response

from kaiju_tools.rpc import RPCClientService


class MessageService(RPCClientService):
    service_name = "CORE_CALLER"

    def __init__(self, *args, **kws):
        super().__init__(*args, **kws)

    async def send(self, user_id: uuid.UUID, chat_id: uuid.UUID, data: str,
                   type_: str = "DEFAULT",
                   attachment: list = None,
                   tags: List[str] = None):
        data = await self.call(method="Messenger.message.send",
                               params={"user_id": user_id,
                                       "chat_id": chat_id,
                                       "data": data,
                                       "type": type_,
                                       "attachment": attachment,
                                       "tags": tags})
        return data

    async def search_by_tags(self, chat_id: uuid.UUID, message_info: MessageInfo, tag: str):
        data = await self.call(method="Messenger.message.search_by_tags",
                               params={"chat_id": chat_id, "message_info": message_info, "tag": tag})
        return data

    async def list_by_message_id(self, chat_id: uuid.UUID, message_info: MessageInfo = None, user_id: uuid.UUID = None,
                                 direction: bool = True) -> Response:
        data = await self.call(method="Messenger.message.list_by_id",
                               params={"user_id": user_id,
                                       "chat_id": chat_id,
                                       "message_info": message_info,
                                       "direction": direction})
        return data

    async def list(self, user_id: uuid.UUID, chat_id: uuid.UUID, paging: bool = False) -> Response:
        data = await self.call(method="Messenger.message.list",
                               params={"user_id": user_id,
                                       "chat_id": chat_id,
                                       "paging": paging})
        return data

    async def delete(self, chat_id: uuid.UUID, user_id: uuid.UUID, message_info: MessageInfo):
        data = await self.call(method="Messenger.message.delete",
                               params={"user_id": user_id,
                                       "chat_id": chat_id,
                                       "message_info": message_info})
        return data

    async def update(self, user_id: uuid.UUID, chat_id: uuid.UUID, message_info: MessageInfo, data: str):
        data = await self.call(method="Messenger.message.update",
                               params={"user_id": user_id,
                                       "chat_id": chat_id,
                                       "message_info": message_info,
                                       "data": data})
        return data

    async def mark_as_read(self, chat_id: uuid.UUID, user_id: uuid.UUID, message_info: MessageInfo):
        data = await self.call(method="Messenger.message.mark_as_read",
                               params={"chat_id": chat_id, "user_id": user_id, "message_info": message_info})
        return data

    async def mark_as_arrived(self, chat_id: uuid.UUID, user_id: uuid.UUID, message_info: MessageInfo):
        data = await self.call(method="Messenger.message.mark_as_arrived",
                               params={"chat_id": chat_id, "user_id": user_id, "message_info": message_info})
        return data

