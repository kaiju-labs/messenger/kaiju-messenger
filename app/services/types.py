from uuid import UUID
from typing import TypedDict, List, Optional, Union
from datetime import datetime


class Attachment(TypedDict):
    attachment_id: UUID
    tags: List[str]
    file: str
    message_id: UUID
    chat_id: UUID
    created_time: datetime


class MessageInfo(TypedDict):
    message_id: UUID
    bucket: int


class AttachmentInfo(TypedDict):
    attachment: UUID
    tags: List[str]


class MessageStatus(TypedDict):
    status: int
    list_user_who_read: Optional[List[UUID]]


class Message(TypedDict):
    message_info: MessageInfo
    chat_id: UUID
    owner_id: UUID
    status: MessageStatus
    count_in_bucket: int
    attachment: Optional[List[UUID]]
    data: str
    created_time: datetime
    updated_time: Optional[datetime]


class BaseData(TypedDict):
    pass


class Status(str):
    pass


class EmptyData(List):
    pass


class Response(TypedDict):
    data: Union[BaseData, EmptyData]
    status: Union[Status, str]


class GroupCreateData(BaseData):
    chat_id: UUID


class GroupInfo(TypedDict):
    settings: TypedDict
    owner_id: UUID
    name: str
    created: datetime
    clients_list: List[UUID]


class GroupListData(BaseData):
    chat_list: List[GroupInfo]


class GroupLeaveData(BaseData):
    clients_list: List[UUID]


class GroupInviteData(BaseData):
    clients_list: List[UUID]


class GroupDeleteData(BaseData):
    chat_list: List[GroupInfo]


class GroupUpdateData(BaseData):
    chat_list: List[GroupInfo]


class AttachmentData(BaseData):
    message: List[Attachment]


class MessageListData(BaseData):
    message_list: List[Message]


class MessageSendData(BaseData):
    message_id: UUID


class MessageSearchTagsData(BaseData):
    message_list: List[Message]


class MessageListByIDData(BaseData):
    message_list: List[Message]


class MessageDeleteData(BaseData):
    pass


class MessageUpdateData(BaseData):
    pass


class MessageMarkReadData(BaseData):
    pass


class MessageMarkArrivedData(BaseData):
    pass
