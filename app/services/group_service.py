import uuid
from .types import *

from kaiju_tools.rpc import RPCClientService


class MessengerGroupService(RPCClientService):
    service_name = "GROUP_CALLER"

    def __init__(self, *args, **kws):
        super().__init__(*args, **kws)

    async def create(self, chat_name: str, type_chat: str, user_id: uuid.UUID, inviting_clients=None) -> Response:
        return await self.call(method="MessengerGroup.create",
                               params={"chat_name": chat_name,
                                       "type_chat": type_chat,
                                       "user_id": user_id,
                                       "inviting_clients": inviting_clients})

    async def list(self, user_id: uuid.UUID) -> Response:
        return await self.call(method="MessengerGroup.list",
                               params={"user_id": user_id})

    async def leave(self, user_id: uuid.UUID, chat_id: uuid.UUID) -> Response:
        return await self.call(method="MessengerGroup.leave",
                               params={"chat_id": chat_id, "user_id": user_id})

    async def invite(self, user_id: uuid.UUID, inviting_user_id: uuid.UUID, chat_id: uuid.UUID) -> Response:
        return await self.call(method="MessengerGroup.invite",
                               params={"inviting_user_id": inviting_user_id, "user_id": user_id,
                                       "chat_id": chat_id})

    async def delete(self, user_id: uuid.UUID, chat_id: uuid.UUID) -> Response:
        return await self.call(method="MessengerGroup.delete",
                               params={"user_id": user_id,
                                       "chat_id": chat_id})

    async def update_name(self, user_id: uuid.UUID, chat_id: uuid.UUID, new_name: str) -> Response:
        return await self.call(method="MessengerGroup.new_name",
                               params={"user_id": user_id,
                                       "chat_id": chat_id,
                                       "new_name": new_name})

    async def settings(self, user_id: uuid.UUID, chat_id: uuid.UUID, settings: dict) -> Response:
        return await self.call(method="MessengerGroup.settings",
                               params={"user_id": user_id,
                                       "chat_id": chat_id,
                                       "settings": settings})

    async def client_list(self, user_id: uuid.UUID, chat_id: uuid.UUID) -> Response:
        return await self.call(method="MessengerGroup.clients_list",
                               params={"user_id": user_id,
                                       "chat_id": chat_id})

    async def change_owner(self, user_id: uuid.UUID, chat_id: uuid.UUID, new_owner_id: uuid.UUID):
        return await self.call(method="MessengerGroup.change_owner",
                               params={"user_id": user_id,
                                       "chat_id": chat_id,
                                       "new_owner_id": new_owner_id})

    async def info(self, chat_id: uuid.UUID):
        return await self.call(method="MessengerGroup.info",
                               params={"chat_id": chat_id})
