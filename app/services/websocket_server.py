from typing import List
from kaiju_tools.encoding.json import dumps, loads
from kaiju_tools.fixtures import ContextableService

import asyncio


def timeout_task(func):
    async def inner(self, data):
        try:
            await asyncio.wait_for(func(self, data), timeout=self.timeout)
        except asyncio.TimeoutError:
            self.logger.warning(f"Timeout: {data.get('method')}")

    return inner


class WebsocketWorker:
    def __init__(self, websocket, services, logger, timeout, callback):
        self.logger = logger
        self.ws = websocket
        self.user_id = None
        self.background_tasks = set()
        self.services = services
        self.timeout = timeout
        self.callback = callback
        self.commands = self.services["TransportSession"].routes
        self.redirected_method = self.services["TransportSession"].redirected_method

    async def get_new_data_form_chat(self, chat_id):
        function = self.commands['message.list']
        try:
            temp = await asyncio.wait_for(function(user_id=self.user_id,
                                                   chat_id=chat_id,
                                                   paging=False),
                                          timeout=self.timeout)
            temp["method"] = "message.list"
        except asyncio.TimeoutError:
            self.logger.warning(f"Timeout: message.list")
            return

        if not self.ws.closed:

            await self.ws.send_json(temp, dumps=dumps)
            while temp["status"] != "FINISH" and not self.ws.closed:
                try:
                    temp = await asyncio.wait_for(function(user_id=self.user_id,
                                                           chat_id=chat_id,
                                                           paging=True),
                                                  timeout=self.timeout)
                    temp["method"] = "message.list"
                    self.logger.debug("MESSAGE_LIST ")
                except asyncio.TimeoutError:
                    self.logger.warning(f"Timeout: message.list")
                else:
                    if not self.ws.closed:
                        await self.ws.send_json(temp, dumps=dumps)

    @timeout_task
    async def task(self, data):
        value = {
            'response_id': data.get('request_id', 'None'),
            'method': data.get('method'),
            **(await self.commands[data.get('method')](**(data.get('params'))))
        }

        if 'chat_id' in data.get('params') and value['status'] == "OK" and data.get(
                'method') in self.redirected_method:
            self.logger.debug("Redirect method %s", data.get('method'))
            await self.services["RedisPubService"].publish(str(data.get('params')['chat_id']), dumps(value))
        else:
            if not self.ws.closed:
                await self.ws.send_json(value, dumps=dumps)

    @property
    def get_ws(self):
        return self.ws

    async def method(self, data):
        if data.get('method') in self.commands and self.user_id is not None:
            data['params']['user_id'] = self.user_id
            temp_task = asyncio.create_task(self.task(data))
            self.background_tasks.add(temp_task)
            temp_task.add_done_callback(self.background_tasks.discard)
        if data.get('method') == 'new_data':
            chat_list = await self.services["TransportSession"].chat_list(self.user_id)
            value = {
                "response_id": "",
                'method': "chat.list",
                **chat_list
            }
            if not self.ws.closed:
                await self.ws.send_json(value, dumps=dumps)
                for i in chat_list['data']['chat_list']:
                    temp_task = asyncio.create_task(self.get_new_data_form_chat(i["chat_id"]))
                    self.background_tasks.add(temp_task)
                    temp_task.add_done_callback(self.background_tasks.discard)
        if data.get('method') == 'login':
            response, self.user_id, chat_list = await self.services["TransportSession"].login(
                data.get('params')['access_token']
            )
            self.logger.debug("LOGIN %s %s %s", response, self.user_id, chat_list)
            if not self.ws.closed:
                await self.ws.send_json({"response_id": data.get('request_id'),
                                         "method": "login",
                                         **response}, dumps=dumps)
            if self.user_id is not None:
                await self.callback(self, chat_list)

    async def new_chat(self, chat_id):
        chat_list = await self.services["TransportSession"].chat_info(chat_id)
        value = {
            "response_id": "",
            'method': "chat.list",
            **chat_list
        }
        if not self.ws.closed:
            await self.ws.send_json(value, dumps=dumps)
            temp_task = asyncio.create_task(self.get_new_data_form_chat(chat_id))
            self.background_tasks.add(temp_task)
            temp_task.add_done_callback(self.background_tasks.discard)

    def __del__(self):
        pass

    def __repr__(self):
        return str(self.user_id)

    @property
    def get_user_id(self):
        return self.user_id

    async def close(self):
        self.logger.debug(f"closing connection %s", self.user_id)

        for i_task in self.background_tasks:
            i_task.cancel()
            try:
                await i_task
            except asyncio.CancelledError:
                self.logger.warning(f"closing task {i_task}")


class WebSocketServerService(ContextableService):
    service_name = "WebSocketServer"

    def __init__(self, app,
                 transport_service: str = None,
                 redis_pub_service: str = None,
                 redis_handler_service: str = None,
                 method_call_timeout: float = 10,
                 logger=None, **_):
        super().__init__(app=app, logger=logger)
        self.RedisHandler = None
        self.services = dict()
        self.websocket_workers: List[WebsocketWorker] = []
        self.transport_service = transport_service
        self.redis_pub_service = redis_pub_service
        self.redis_handler_service = redis_handler_service
        self.method_call_timeout = method_call_timeout
        self.dict_websocket_worker = dict()
        self.logger.info("WEBSOCKET SERVER")

    async def init(self):
        self.RedisHandler = self.discover_service(self.redis_handler_service, cls=None, required=True)
        self.services["RedisPubService"] = self.discover_service(self.redis_pub_service, cls=None, required=True)
        self.services["TransportSession"] = self.discover_service(self.transport_service, cls=None, required=True)

    def create_worker(self, ws) -> WebsocketWorker:
        temp = WebsocketWorker(ws, self.services, self.logger, self.method_call_timeout, self.callback_update_ws_worker)
        self.websocket_workers.append(temp)
        return temp

    def __contains__(self, key) -> bool:
        return key in self.dict_websocket_worker

    async def callback_update_ws_worker(self, ws_worker: WebsocketWorker, chat_list):
        if ws_worker.get_user_id in self.dict_websocket_worker:
            self.dict_websocket_worker[ws_worker.get_user_id].append(ws_worker)
        else:
            self.dict_websocket_worker[ws_worker.get_user_id] = [ws_worker]
            await self.RedisHandler.subscribe(chat_list, ws_worker.get_user_id)
        self.logger.debug("Dict authorized users %s", self.dict_websocket_worker)

    def __getitem__(self, item):
        result = []
        if item in self.dict_websocket_worker:
            for worker in self.dict_websocket_worker[item]:
                result.append(worker.get_ws)
        return result

    def get_worker(self, user_id) -> WebsocketWorker:
        return self.dict_websocket_worker.get(user_id, [])

    async def close_worker(self, worker: WebsocketWorker) -> None:
        if worker.get_user_id in self.dict_websocket_worker:
            if worker in self.dict_websocket_worker[worker.get_user_id]:
                self.dict_websocket_worker[worker.get_user_id].remove(worker)
            if not self.dict_websocket_worker[worker.get_user_id]:
                self.dict_websocket_worker.pop(worker.get_user_id)
            chat_list = await self.services["TransportSession"].chat_list(worker.get_user_id)
            for i in chat_list['data']['chat_list']:
                await self.RedisHandler.unsubscribe(str(i["chat_id"]), worker.get_user_id)

            self.websocket_workers.remove(worker)
        await worker.close()

    @property
    def service_info(self):
        self.logger.info({
            "list_worker": self.websocket_workers,
            "dict_worker": self.dict_websocket_worker,
            "count_worker": len(self.websocket_workers),
            "count_auth_worker": len(self.dict_websocket_worker),
        })
        return {
            "list_worker": self.websocket_workers,
            "dict_worker": map(lambda x: (x, len(self.dict_websocket_worker[x])), self.dict_websocket_worker.keys()),
            "count_worker": len(self.websocket_workers),
            "count_auth_user": len(self.dict_websocket_worker),
        }
