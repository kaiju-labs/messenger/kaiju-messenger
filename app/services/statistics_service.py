import asyncio
import uuid as uuid
from kaiju_tools.fixtures import ContextableService
from kaiju_tools.rpc import AbstractRPCCompatible
from kaiju_tools.encoding.json import dumps, loads


class StatisticsService(ContextableService, AbstractRPCCompatible):
    service_name = 'MessengerStatistics'

    def __init__(
            self, app, websocket_service, redishandler_service, admin_key = None,
            logger=None):
        super().__init__(app=app, logger=logger)
        self.websocket_service = None
        self.redishandler_service = None
        self.websocket_service_name = websocket_service
        self.redishandler_service_name = redishandler_service

    @property
    def routes(self):
        routes = {
            'websocket_server': self.websocket_service_info,
            'redis_handler': self.redishandler_service_info,
            # 'redis_pub_sub': self.redispubsub_service_info,
        }
        return routes

    @property
    def permissions(self):
        return {
            self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_GUEST_PERMISSION
        }

    async def init(self):
        self.websocket_service = self.discover_service(self.websocket_service_name)
        self.redishandler_service = self.discover_service(self.redishandler_service_name)

    async def websocket_service_info(self, admin_key):
        return self.websocket_service.service_info

    async def redishandler_service_info(self):
        return self.redishandler_service.service_info
    #
    # async def redispubsub_service_info(self, admin_token):
    #     pass
