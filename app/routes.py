"""
Write your routes here.

`urls` - url list which will be registered in the app's router
"""


from .views import *
from kaiju_tools.http.views import JSONRPCView

urls = [
    ('*', '/ws', WebSocketServer, 'socket_server'),
    ('*', '/public/rpc', JSONRPCView, 'json_rpc_view')
]
