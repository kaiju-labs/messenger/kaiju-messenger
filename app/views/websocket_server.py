from aiohttp.web import View
from aiohttp import web
from aiohttp import WSMsgType as MsgType
from kaiju_tools.encoding.json import dumps, loads
import asyncio


class WebSocketServer(View):
    async def get(self):
        ws = web.WebSocketResponse(autoclose=True, heartbeat=10)
        await ws.prepare(self.request)
        ws_worker = self.request.app.services.WebSocketServer.create_worker(ws)
        try:
            async for msg in ws:
                if msg.type == MsgType.text:
                    try:
                        data = loads(msg.data)
                    except:
                        await ws.send_str(f" NOT JSON {msg.data}")
                    else:
                        await ws_worker.method(data)
                if msg.type == MsgType.close:
                    await ws.close()
        except (asyncio.CancelledError, asyncio.TimeoutError):
            self.request.app.services.WebSocketServer.logger.debug(f"Error Timeout receive answer")
        await self.request.app.services.WebSocketServer.close_worker(ws_worker)
        return ws
